---
layout: secure_and_protect_direction
title: "Category Direction - Web Application Firewall"
description: "By being able to see what is being sent to your systems, GitLab wants to empower you to either block malicious traffic or to otherwise act on it."
canonical_path: "/direction/protect/web_application_firewall/"
---

- TOC
{:toc}


| | |
| --- | --- |
| Stage | [Protect](/direction/protect) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2020-10-22` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category direction page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/product-categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->
Thanks for visiting this category direction page on the Web Application Firewall (WAF) in GitLab. This page belongs to the Container Security group of the Protect stage and is maintained by Sam White ([swhite@gitlab.com](mailto:<swhite@gitlab.com>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWAF) and [epics](https://gitlab.com/groups/gitlab-org/-/epics/795) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for a web application firewall, we'd especially love to hear from you.

### Overview
A web application firewall (WAF) filters, monitors, and blocks web traffic to and from a web application. A WAF is differentiated from a regular firewall in that a WAF is able to filter the content of specific web applications, while regular firewalls serve as a safety gate between servers. By inspecting the contents of web traffic, it can prevent attacks stemming from web application security flaws, such as SQL injection, cross-site scripting (XSS), file inclusion, and security misconfigurations.

GitLab's WAF comes with a default out-of-the-box OWASP ruleset in detection only mode.

### Where we are Headed

GitLab's WAF will be [deprecated](https://gitlab.com/gitlab-org/gitlab/-/issues/271276) as of our %13.6 release and will be [removed](https://gitlab.com/gitlab-org/gitlab/-/issues/271349) from the product in our %14.0 release.  Please reference [this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/271276) for additional details.
