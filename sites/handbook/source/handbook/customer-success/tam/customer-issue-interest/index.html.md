---
layout: handbook-page-toc
title: "Capturing Customer Interest in GitLab Issues"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

This page has been deprecated and information previously held here can be found on the [TAM & Product Interaction handbook page](/handbook/customer-success/tam/product/).
