// webpack.config.js
const webpack = require('webpack');
const path = require('path');
const glob = require('glob');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries")

// Captures all scss files
const mapStylesheets = pattern => glob
  .sync(pattern)
  .reduce((entries, filename) => {
    const [, name] = filename.match(/([^/]+)\.scss$/)
    return { ...entries, [name]: filename }
  }, {})

module.exports = {

  // Catch all files within stylesheets directory
  entry: {
    ...mapStylesheets('./source/stylesheets/*.*.scss'),
  },

  // Output all javascript to specific directory

  output: {
    path: path.join(__dirname, '/tmp/dist'),
  },

  resolve: {
    modules: [
      "node_modules"
    ]
  },


  module: {
    rules: [
      // Extracts the compiled CSS from the SASS files defined in the entry
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            // Interprets CSS
            loader: "css-loader",
            options: {
              importLoaders: 2
            }
          },
          {
            loader: 'sass-loader' // 将 Sass 编译成 CSS
          }
        ]
      }
    ],
  },

  plugins: [
    // Where the compiled SASS is saved to
    // Note: If [name].css is included in filename it outputs .css.css for css.scss files
    // Todo: [name] will not capture just filename.scss files such as blog-landing.css
    new MiniCssExtractPlugin({
      filename: "stylesheets/[name]",
      chunkFilename: "stylesheets/[name].css"
    }),
    // Removes js files produced from scss compiling
    new FixStyleOnlyEntriesPlugin()
  ],

  optimization: {
    minimizer: [
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          safe: true
        }
      })
    ]
  },

  stats: {
    entrypoints: false,
    children: false
  },
};
